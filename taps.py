import pandas as pd
import json
import logging
"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"
"__project__ = 'kbc_singer'"

"""
Python 3 environment
"""

DEFAULT_FILE_INPUT = "/data/in/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/tables/"


class taps:
    data = []
    pk = []
    incre = True
    pk_bool = True

    def __init__(self, name, pk, tap_name):
        self.name = name
        self.pk = pk
        self.tap_name = tap_name

    def add_data(self, x):
        self.data.append(x)

    def output(self):
        output_name = self.tap_name+"-"+self.name
        df = pd.DataFrame(self.data)
        df.to_csv(DEFAULT_FILE_DESTINATION+output_name+".csv", index=False)
        self.check_pk(df)
        self.produce_manifest()

    def check_pk(self, df):
        df_column = list(df)
        for i in self.pk:
            if i in df_column:
                pass
            else:
                self.pk_bool = False
        if self.pk_bool:
            logging.info("INFO: {0}'s Primary Key: {1}".format(
                self.name, self.pk))
        else:
            logging.warning(
                "WARNING: {0}: Primary Key is not found.".format(self.name))

    def produce_manifest(self):
        """
        Dummy function to return header per file type.
        """

        output_name = self.tap_name+"-"+self.name
        file = "/data/out/tables/"+str(output_name+".csv")+".manifest"
        destination_part = output_name  # noqa

        manifest_template = {  # "source": "myfile.csv"
            # ,"destination": "in.c-mybucket.table"
            # "incremental": True
            # ,"primary_key": ["VisitID","Value","MenuItem","Section"]
            # ,"columns": [""]
            # ,"delimiter": "|"
            # ,"enclosure": ""
        }

        manifest = manifest_template
        if self.pk_bool:
            manifest["primary_key"] = self.pk
        manifest["incremental"] = self.incre

        try:
            with open(file, 'w') as file_out:
                json.dump(manifest, file_out)
                logging.info("INFO: Output manifest file ({0}) produced.".format(
                    str(self.name)+".manifest"))
        except Exception as e:
            logging.error("ERROR: Could not produce output file ({0}) manifest.".format(
                str(self.name)+".manifest"))
            logging.error(e)
