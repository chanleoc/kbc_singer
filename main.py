from subprocess import Popen, PIPE, STDOUT, call, getstatusoutput  # noqa
import singer  # noqa
from taps import taps
from keboola import docker
import pandas as pd  # noqa
import json
import logging
import os
import sys
import argparse  # noqa
import pip
import logging_gelf.handlers
import logging_gelf.formatters
"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"
"__project__ = 'kbc_singer'"

"""
Python 3 environment 
"""


# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Logging

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

if 'KBC_LOGGER_ADDR' in os.environ and 'KBC_LOGGER_PORT' in os.environ:

    logger = logging.getLogger()
    logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
        host=os.getenv('KBC_LOGGER_ADDR'), port=int(os.getenv('KBC_LOGGER_PORT')))
    logging_gelf_handler.setFormatter(
        logging_gelf.formatters.GELFFormatter(null_character=True))
    logger.addHandler(logging_gelf_handler)

    # remove default logging to stdout
    logger.removeHandler(logger.handlers[0])

"""
###########################
### Sample JSON Config ####
{
    "tap": "tap-source",
    "incremental": true,
    "discovery_mode": false,
    "tap_config": [
        {
        "property": "access_key",
        "type": "string",
        "value": "123456789"
        },
        {
        "property": "access_secret",
        "type": "string",
        "value": "123456789"
        }
    ],
    "tap_state": [],
    "tap_catalog": []
}
###########################
"""

# Access the supplied rules
cfg = docker.Config('/data/')
params = cfg.get_parameters()
incremental = cfg.get_parameters()["incremental"]
tap = cfg.get_parameters()["tap"]
discovery_mode = cfg.get_parameters()["discovery_mode"]
tap_config = cfg.get_parameters()["tap_config"]
tap_state = cfg.get_parameters()["tap_state"]
tap_catalog = cfg.get_parameters()["tap_catalog"]

# destination to fetch and output files
DEFAULT_FILE_INPUT = "/data/in/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/tables/"


def flatten_json(y):
    """
    flat out the json objects
    """
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out


def produce_config(config, filetype):
    """
    Output configuration for Singer Tap if required
    Input: config = list
    """
    data = {}
    filename = 'tap_'+str(filetype)+'.json'
    for item in config:
        property_name = item["property"]
        value_type = item["type"]
        value = item["value"]
        if value_type == "bool":
            if value.lower() == "true":
                data[property_name] = True
            else:
                data[property_name] = False
        else:
            data[property_name] = value

    with open(filename, 'w') as out:
        json.dump(data, out)

    logging.info("INFO: Tap {0} created.".format(filetype))

    return filename


def reproduce_config(data, filetype):
    """
    Output configuration for Singer Tap if required
    Input: data = json formatted data
    """
    # data = {}
    # filename = 'tap_'+str(filetype)+'.json'
    filename = str(filetype)+'.json'

    with open(filename, 'w') as out:
        json.dump(data, out)

    logging.info("INFO: Tap {0} created.".format(filetype))

    return filename


def main():
    """
    Main execution script.
    """

    ############################################################################################################
    # TAP execution #########################################################################################
    pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', tap])

    status, code = getstatusoutput(tap)
    if status == 127 or status == 256:
        raise Exception(
            "Tap, {0}, is not found. Please verify if Tap exists.".format(tap))

    execution = tap
    if discovery_mode:
        # Discovery Mode enabled
        logging.info("INFO: Discovery mode enabled.")
        discover = execution
        if tap_config:
            config_name = produce_config(tap_config, 'config')
            logging.info("INFO: Tap configuration loaded.")
            query = " -c {0}".format(config_name)
            discover = discover + query
        # stemp_discover = discover + " --discover"

        discover = discover + " --discover > properties.json"
        logging.info("INFO: Discover query: {0}".format(discover))
        os.popen(discover).readlines()

        logging.info("INFO: Outputting properties.json")

        # modify properties for "tap-mysql" ONLY
        # if tap == "tap-mysql":
        with open("properties.json", 'r') as file:
            properties = json.load(file)
        logging.info("INFO: Properties: {0}".format(properties))

        for stream in properties["streams"]:
            stream["schema"]["selected"] = "true"
            for item in stream["schema"]["properties"]:
                stream["schema"]["properties"][item]["selected"] = "true"
        reproduce_config(properties, "properties")

        execution = execution + \
            " -c {0} -p properties.json".format(config_name)

    else:
        # Discovery Mode disabled
        if tap_config:
            config_name = produce_config(tap_config, 'config')
            logging.info("INFO: Tap configuration loaded.")
            query = " -c {0}".format(config_name)
            execution = execution + query
        if tap_catalog:
            catalog_name = produce_config(tap_state, 'catalog')
            logging.info("INFO: Tap catalog loaded.")
            query = " -p {0}".format(catalog_name)
            execution = execution + query
        if not tap_config and not tap_state and not tap_catalog:
            logging.info("INFO: No Tap Configurations found.")

    if tap_state:
        state_name = produce_config(tap_state, 'state')
        logging.info("INFO: Tap state loaded.")
        query = " -s {0}".format(state_name)
        execution = execution + query

    logging.info("INFO: Execution Query: {0}".format(execution))
    # pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', tap])
    output = os.popen(execution).readlines()

    ############################################################################################################
    # Processing TAP data ####################################################################################
    files_saved = {}
    for line in output:
        sys.stdout.write(line)
        logging.info(line)
        # Loading line as JSON
        try:
            line = json.loads(line)
        except Exception as err:
            logging.error("ERROR: Unable to parse {0} - {1}".format(line, err))
            raise

        if "type" not in line:
            raise Exception(
                "ERROR: Line is missing required key 'type': {0}".format(line))

        # Case 1: Line type =  RECORD
        # Output: Data Files Only
        if line["type"] == "RECORD":

            # Validating stream name
            if "stream" not in line:
                raise Exception("ERROR: stream is missing: {0}".format(line))
            filename = line["stream"]
            file_out = filename + ".csv"

            # Validating records
            if "record" not in line:
                raise Exception("ERROR: record is missing; {0}".format(line))
            flat_data = flatten_json(line["record"])
            header = flat_data.keys()  # noqa

            files_saved[filename].add_data(flat_data)

        # Case 2: Line type =  SCHEMA
        # Output: Individual file data & Manifest
        elif line["type"] == "SCHEMA":

            # Validating stream name
            if "stream" not in line:
                raise Exception("ERROR: stream is missing: {0}".format(line))
            filename = line["stream"]
            file_out = filename + ".csv"  # noqa

            # Validating primary key
            if "key_properties" not in line:
                raise Exception(
                    "ERROR: key_properties is missing: {0}".format(line))
            pk = line["key_properties"]

            # Validate stream
            if filename not in files_saved:
                files_saved[filename] = taps(
                    name=filename, pk=pk, tap_name=tap)
                if not incremental:
                    files_saved[filename].incre = False
            else:
                raise Exception(
                    "ERROR: Duplicated SCHEME for {0}('stream'): {1}".format(line["stream"], line))

        # Case 3: Line type = STATE
        elif line["type"] == "STATE":
            pass

        elif line["type"] == "ACTIVATE_VERSION":
            pass

        # Case 4: invalid type
        else:
            raise Exception(
                "ERROR: Input row is an unknown type: {0}".format(line))

    for i in files_saved:
        current = (files_saved[i])
        current.output()

    logging.info("INFO: {0}".format(files_saved))

    return


if __name__ == "__main__":

    main()

    logging.info("INFO: Done.")
