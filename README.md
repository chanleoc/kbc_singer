# Keboola Singer Tap Extractor #

### Introduction ###
This repository is a collection of functions which allow users to use [Singer](https://www.singer.io/#what-it-is) taps as an extractor and export outputs directly into KBC storage. It provides users the flexibility to explore, manipulate and enrich the data before they push the data to any systems and storages. Combining with KBC orchestration, users will be able to automate this process with a specified times of the day. 

### Configuration ###

1. Tap Name
    - A Singer tap which contains scripts of data extraction
    - Component will be terminated if the defined Tap is not found

2. Output Bucket
    - Tap name will be used to define the destination of the output bucket. File name will be automically generated from the name of the stream.
    - This is not configurable 

3. Incremental Load
    - Loading the output tables incrementally into destinated KBC storage
    - New files will be generated if destinated output files are not found

4. Discovery Mode
    - Outputting the list of available streams to properties.json which will later be used as a selected template for data request
    - If enabled, any inputs in Tap Catalog will be ignored. The streams and schema fetched from Discovery Mode will be used as the new properties/catalog
    - Please ensure if the taps support discovery mode; if not, please leave Discovery Mode **FALSE**.

5. Tap Configuration
    - A JSON formatted configuration containing the necessary configuration parameters the Tap needs
    - If no configurations are needed, please leave this section blank
    - Component will use the defined variables to construct a JSON object with property (in configuration) as the JSON object property and value as the JSON object value

6. Tap State
    - A JSON formatted configuration containing information of previous run.
    - If there is not any state information, please leave this section blank
    - Component will use the defined variables to construct a JSON object with property (in configuration) as the JSON object property and value as the JSON object value

7. Tap Catalog
    - A JSON formatted configuration used to filter which streams should be synced
    - If there is not any catalog information, please leave this section blank
    - Component will use the defined variables to construct a JSON object with property (in configuration) as the JSON object property and value as the JSON object value

**Note: Component will construct the requirements of the taps from any inputted information from tap configuration, tap states and tap catalog. However, it is users' responsiblity to determine if the defined tap component requires any configuration or how the tap should be configured. Author of this repository nor KBC will provide any technical supports on any non-functional taps. Users will need to contact the tap author for any sort of configuration issues.**

### Known Non-Working Taps ###
*Known taps that do not work on this platform*
##
- tap-mysql

### Alternatives ###
If users have a pre-configured configurations, states or catalogs, and wish to run the taps locally, they can use our [target-kbc](https://bitbucket.org/chanleoc/target-keboola) to output the tables into our storage. Detailed requirements and configurations can be found in the [target-kbc documentations](https://bitbucket.org/chanleoc/target-keboola).

### RAW JSON Configuration ###

```
{
    "tap": "tap-source",
    "incremental": true,
    "discovery_mode": false,
    "tap_config": [
        {
        "property": "access_key",
        "type": "string",
        "value": "123456789"
        },
        {
        "property": "access_secret",
        "type": "string",
        "value": "123456789"
        }
    ],
    "tap_state": [],
    "tap_catalog": []
}
```

### Contact Info ###

Leo Chan  
Vancouver, Canada (PST time)  
Email: leo@keboola.com  
Private: cleojanten@hotmail.com  
